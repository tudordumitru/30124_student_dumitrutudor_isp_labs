package isp.lab5.exercise3;


public class BlackAndWhiteVideo extends ColorVideo {
    private ColorVideo video;
    public String fileName;

    public BlackAndWhiteVideo(String fileName) {
        this.fileName = fileName;
    }

    public void play() {
        if(video == null){
            video = new ColorVideo(fileName);

        }
        System.out.println("Play black and white video" + fileName);
    }
}