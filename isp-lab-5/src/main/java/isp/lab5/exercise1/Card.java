package isp.lab5.exercise1;

public class Card {private String cardld;
    private String pin;

    public Card(String cardld, String pin) {
        this.cardld = cardld;
        this.pin = pin;
    }

    public String getCardId() {
        return cardld;
    }

    public void setCardId(String cardld) {
        this.cardld = cardld;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
