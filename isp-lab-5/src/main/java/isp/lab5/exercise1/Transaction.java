package isp.lab5.exercise1;

public class Transaction { protected Account account;

    public Transaction(Account account) {
        this.account = account;
    }

    abstract String execute();
}
