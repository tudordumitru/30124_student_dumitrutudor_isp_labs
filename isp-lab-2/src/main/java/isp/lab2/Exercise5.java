package isp.lab2;
import java.util.Random;
import java.lang.Math;
public class Exercise5 {

    /**
     * This method should generate an array that have 20 random numbers between -1000 and 1000
     *
     * @return the random numbers
     */
    public static int[] generateRandomNumbers() {
    Random rand=new Random();
    int[] array= new int[20];
    int a=0;
    for(int i=0;i<=19;i++)
    {a= -1000+rand.nextInt(2000);
    array[i]=a;
    }
       return array;


    }

    /**
     * This method should sort the given random numbers
     *
     * @param randomNumbers numbers generated random
     * @return sorted int array
     */
    public static int[] getSortedNumbers(int[] randomNumbers) {
       int aux=0;
        for(int i=0;i<randomNumbers.length;i++)
            for(int j=i+1;j<randomNumbers.length;j++)
                if(randomNumbers[i]>randomNumbers[j])
                {
                    aux=randomNumbers[j];
                    randomNumbers[j]=randomNumbers[i];
                    randomNumbers[i]=aux;

                }
        return randomNumbers;
    }

    public static void main(String[] args) {
        // display the random generated numbers
        int[] randomNumbers = generateRandomNumbers();
        System.out.println("The random generated numbers are:");
        for (int i = 0; i < randomNumbers.length; i++) {
            System.out.print(randomNumbers[i] + ", ");
        }
        int[] sortedNumbers = getSortedNumbers(randomNumbers);
        // display the sorted numbers
        System.out.println("The sorted numbers are:");
        for (int i = 0; i < sortedNumbers.length; i++) {
            System.out.print(sortedNumbers[i] + ", ");
        }
    }
}
