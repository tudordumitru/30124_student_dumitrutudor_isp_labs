package isp.lab2;

public class Exercise3 {

    /**
     * This method should verify if a number is prime
     *
     * @param number the number to check
     * @return true if number is prime and false otherwise
     */
    public static boolean isPrimeNumber(final int number) {
        if(number==0)
            return false;
        if(number==1)
            return false;
        if(number==2)
            return true;

        int d;
        for(d=number/2;d>1;d--)
            if(number%d==0)
                return false;
        return true;
    }

    /**
     * This method should calculate the sum of digits of a given number
     *
     * @param number the number used to calculate the sum of digits
     * @return an int representing the sum of digits of the given number
     */
    public static int calculateSumOfDigits(int number) {
        int s=0;
        while(number!=0) {
            s += number % 10;
            number=number/10;
        }
        return s;
    }

    /**
     * This method should extract the prime numbers from a given interval
     * using isPrimeNumber(final int number) method defined above
     * NOTE* a < b
     *
     * @param a the left end of the interval
     * @param b the right end of the interval
     * @return and int array consisting of the prime numbers from the given interval
     */
    public static int[] getPrimeNumbersFromInterval(int a, int b) {
        int[] array=new int[200] ;
        int t=-1;
        for(int i = a;i<=b;i++)
            if(isPrimeNumber(i)==true)
                array[++t]=i;
        int[] array2 = new int[t+1];
        for(int i=0;i<=t;i++)
            array2[i]=array[i];

        return array2;
    }

    /**
     * This method should calculate the geometric mean of the given prime numbers
     *
     * @param primeNumbers the numbers used to calculate the geometric mean
     * @return the geometric mean of the given numbers
     */
    public static double calculateGeometricMean(int[] primeNumbers) {
      double p=1;
       for(int i=0;i<primeNumbers.length;i++)
           p=p*primeNumbers[i];
       return Math.pow(p,1.0/(double)primeNumbers.length);
    }

    /**
     * This method should calculate the number of prime numbers which
     * have the sum of digits an even number
     * NOTE* use calculateSumOfDigits(int number)
     *
     * @param primeNumbers prime numbers used for calculation
     * @return the numbers which respect the given condition
     */
    public static int numberOfPNWithEvenSumOfDigits(int[] primeNumbers) {
        int c=0;
        for(int i=0;i<primeNumbers.length;i++)
            if(calculateSumOfDigits(primeNumbers[i])%2==0)
                c++;
        return c;

    }

    public static void main(String[] args) {
        int a = 0;
        int b = 0;

        System.out.println("The geometric mean is: " + calculateGeometricMean(getPrimeNumbersFromInterval(0, 50)));

        System.out.println("The number of prime numbers which have the sum of digits an even number is: " + numberOfPNWithEvenSumOfDigits(getPrimeNumbersFromInterval(a, b)));

    }
}
