package isp.lab2;
import java.util.Scanner;
public class Exercise6 {

    /**
     * This method should generate the required vector non-recursively
     *
     * @param n the length of the generated vector
     * @return the generated vector
     */
    public static int[] generateVector(int n) {



       int a=1;
       int b=2;
       int[] array = new int[n];
       array[0]=1;
       array[1]=2;
        for(int i=2;i<n;i++)
        {
            array[i]=a*b;
            a=b;
            b=array[i];
        }
        return array;

    }

    /**
     * This method should generate the required vector recursively
     *
     * @param n the length of teh generated vector
     * @return the generated vector
     */
    public static int[] generateRandomVectorRecursively(int n) {

        if (n==1){
            int[] array=new int[n];
            array[0]=1;
            return array;}
        else if(n==2){
            int[] array=new int[n];
            array[0]=1;
            array[1]=2;
            return array;}
        else {
            int[] array=new int[n];
            array=generateRandomVectorRecursively(n-1);
            array[n-1]=array[n-2]*array[n-3];
            return array;
        }


    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n=s.nextInt();
        int[] array = new int[n];

        array=generateRandomVectorRecursively(n);
        System.out.println("A recursive type: ");
        for(int vec:array)
            System.out.print(vec +", ");

    }
}
