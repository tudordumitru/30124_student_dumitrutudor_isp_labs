package isp.lab2;

import java.util.Scanner;

public class Exercise1 {

    /**
     * This method should generate an random number between 2 and 10 then
     * should ask user to enter that amount of numbers from keyboard and
     * store the numbers in an int array which should be returned
     *
     * @return the array of numbers read from keyboard
     */

    private static int[] getUserNumbers() {
        Scanner sc = new Scanner(System.in);
        int n=randomGenerator(2,10);
        System.out.println(n);
        int[] numbers = new int[n+1];
        for (int i = 0; i < n; i++) {
            int num = sc.nextInt();
            numbers[i] = num;
        }
        sc.close();
        return numbers;
    }

    public static int randomGenerator(int min, int max){
        return (int)((Math.random()*(max - min) + min));
    }

    /**
     * This method should compute the arithmetical mean of the given numbers
     *
     * @param userNumbers the numbers used to calculate the arithmetical mean
     * @return the arithmetical mean of the given numbers
     */
    protected static double aritmeticalMean(int[] userNumbers) {

        double average = 0;
        int size = 0;
        int l=userNumbers.length;
        while(l != 0){
            l--;
            average+=userNumbers[size];
            size++;
        }
        average/=size;
        return average;
    }

    public static void main(String[] args) {

        int[] userNumbers = getUserNumbers();
        for (int i = 0; i < userNumbers.length-1 ; i++) {
            System.out.println(userNumbers[i]);
        }
        System.out.println("Mean number is: " + aritmeticalMean(userNumbers));
    }


}
