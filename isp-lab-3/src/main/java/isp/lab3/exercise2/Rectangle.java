package isp.lab3.exercise2;

public class Rectangle {
    private int length = 2;
    private int width = 1;
    private String color = "red";
    public Rectangle(int l,int w){
        length = l;
        width = w;
    }
    public Rectangle(int l,int w,String c){
        length = l;
        width = w;
        color =c;
    }
    public int getLength(){
        return length;
    }


    public int getWidth(){
        return width;

    }

    public String getColor(){
        return color;
    }

    public int getPerimeter()
    {
    return 2*length+(2*width);

    }
    public int getArea(){
        return length*width;

    }
    public static void main(String[] args) {
    Rectangle camera = new Rectangle(10,5,"green");

    System.out.println(camera.getArea() + " " + camera.getColor()+" " + camera.getLength()+" "+camera.getPerimeter() +" "+ camera.getWidth());





    }

}
