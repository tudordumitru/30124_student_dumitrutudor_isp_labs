package isp.lab3.exercise4;

public class MyPoint {

    public int x,y,z;
   MyPoint(){
        x=0;
        y=0;
        z=0;
    }
    MyPoint(int x1,int y1,int z1){
       x=x1;
       y=y1;
       z=z1;
    }
    int getX(){
       return x;
    }
    int getY(){
       return y;
    }
    int getZ(){
       return z;
    }
    void setX(int a){
       x=a;
    }
    void setY(int b){
       y=b;
    }
    void setZ(int c){
       z=c;
    }
    void setXYZ(int a,int b,int c){
       x=a;
       y=b;
       z=c;
    }

    @Override
    public String toString() {
        return "("+x+","+y+","+z+")";
    }
    double distance(int a,int b,int c){
    return Math.sqrt(Math.pow((x-a),2)+Math.pow((y-b),2)+Math.pow((z-c),2));
    }
}
