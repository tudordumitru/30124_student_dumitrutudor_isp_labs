package isp.lab3.exercise3;

public class Vehicle {
    private String model;
    private String type;
    private int speed;
    private char fuelType;
    Vehicle(String m,String t,int s,char f){
        model=m;
        type=t;
        speed=s;
        fuelType=f;

    }
    String getModel(){
        return model;
    }
    String getType(){
        return type;
    }
    int getSpeed(){
        return speed;
    }
    char getFuelType(){
        return fuelType;
    }
    void setModel(String m){
        model=m;
    }
    void setSpeed(int s){
        speed=s;

    }
    void setFuelType(char f){
        fuelType=f;
    }
    void setType(String t){
        type=t;
    }

    @Override
    public String toString() {
        return model + "(" + type + ")"
                 + "speed"+speed + "fuel type" + fuelType ;
    }
    public static void main(String[] args) {
        Vehicle masina = new Vehicle("Dacia","Logan",150,'B');
       System.out.println( masina.toString());

    }
}
