package isp.lab4.exercise1;
import org.junit.Assert;
import org.junit.Test;

public class TemperatureSensorTest {
    TemperatureSensor senzor1 = new TemperatureSensor(5,"Cluj");
    @Test
    public void testGetValue() {

        Assert.assertEquals("Testul Value ",5,senzor1.getValue());
    }
    @Test
    public void testGetLocation() {

        Assert.assertEquals("Testul Location ","Cluj",senzor1.getLocation());
    }
    @Test
    public void testTestToString() {
        Assert.assertEquals("Testul toString ","TemperatureSensor{value=5, location='Cluj'}",senzor1.toString());

    }

}
