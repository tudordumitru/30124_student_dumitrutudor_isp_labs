package isp.lab4.exercise5;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;
import isp.lab4.exercise3.Controler;

import javax.naming.ldap.Control;

public class House {
    Controler controler;




    public House(Controler controler){
        this.controler=controler;
    }
    public Controler getControler(){
        return controler;
    }
    public static void main(String[] args) {



        FireAlarm alarma = new FireAlarm(false);
        TemperatureSensor[] senzor = new TemperatureSensor[3];
        senzor[0] = new TemperatureSensor(41,"Bucatarie");
        senzor[1] = new TemperatureSensor(56,"Hol");
        senzor[2] = new TemperatureSensor(48,"Baie");

        Controler control = new Controler(senzor,alarma);
        House casa = new House(control);
        casa.controler.controlStep();



    }
}
