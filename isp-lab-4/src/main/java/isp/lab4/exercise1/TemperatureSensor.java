package isp.lab4.exercise1;

import isp.lab4.exercise3.Controler;

public class TemperatureSensor  {
     public int value;
     public String location;
    public TemperatureSensor(){
        this.value =0;
        this.location=null;
    }
    public TemperatureSensor(int x,String l )
    {
        this.value=x;
        this.location=l;
    }
    public int getValue(){
        return this.value;
    }
    public String getLocation(){
        return location;
    }

    @Override
    public String toString() {
        return "TemperatureSensor{" +
                "value=" + value +
                ", location='" + location + '\'' +
                '}';
    }
    //atribute
    //constructori
    //metode
}
