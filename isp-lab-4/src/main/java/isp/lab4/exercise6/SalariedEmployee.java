package isp.lab4.exercise6;

public class SalariedEmployee {
    private double weeklySalary;

    //metode
    public SalariedEmployee(double weeklySalary){
        this.weeklySalary=weeklySalary;
    }
    public double getPaymentAmount(){
        return 0.0;
    }

    @Override
    public String toString() {
        return "SalariedEmployee{" +
                "weeklySalary=" + weeklySalary +
                '}';
    }
}
