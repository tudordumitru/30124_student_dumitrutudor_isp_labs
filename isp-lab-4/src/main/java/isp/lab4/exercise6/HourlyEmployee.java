package isp.lab4.exercise6;

public class HourlyEmployee {
    private double wage;
    private double hours;
    //metode
    public HourlyEmployee(double wage,double hours)
    {
        this.wage=wage;
        this.hours=hours;
    }
    public double getPaymentAmount(){
        return 0.0;
    }

    @Override
    public String toString() {
        return "HourlyEmployee{" +
                "wage=" + wage +
                ", hours=" + hours +
                '}';
    }
}
