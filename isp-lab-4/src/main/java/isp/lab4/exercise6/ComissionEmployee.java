package isp.lab4.exercise6;

public class ComissionEmployee {
    private double grossSales;
    private double comissionSales;

    public ComissionEmployee(double grossSales, double comissionSales){
        this.comissionSales = comissionSales;
        this.grossSales = grossSales;
    }
    public double getPaymentAmount(){
        return 0.0;
    }

    @Override
    public String toString() {
        return "ComissionEmployee{" +
                "grossSales=" + grossSales +
                ", comissionSales=" + comissionSales +
                '}';
    }
}
