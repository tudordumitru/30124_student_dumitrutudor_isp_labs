package isp.lab4.exercise6;

import isp.lab4.exercise1.TemperatureSensor;

public class Exercise6 {
    public static void main(String[] args) {
        SalariedEmployee angajat1 = new SalariedEmployee(1000);
        SalariedEmployee angajat2 = new SalariedEmployee(2000);
        HourlyEmployee angajat3 = new HourlyEmployee(1000, 40);
        HourlyEmployee angajat4 = new HourlyEmployee(2000, 80);
        ComissionEmployee angajat5 = new ComissionEmployee(3000, 5000);
        ComissionEmployee angajat6 = new ComissionEmployee(4000, 6000);

        Employee[] angajati = new Employee[6];
        angajati[0] = new Employee("Tudor","Dumitru",angajat1,angajat3,angajat5);
        angajati[1]= new Employee("Dan","Danut",angajat2,angajat4,angajat6);
    }
}
