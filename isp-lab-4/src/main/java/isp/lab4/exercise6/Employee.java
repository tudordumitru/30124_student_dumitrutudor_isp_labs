package isp.lab4.exercise6;

public class Employee {
    private String firstName;
    private String lastName;
    private SalariedEmployee angajatSalariat;
    private HourlyEmployee angajatOra;
    private ComissionEmployee angajatComision;
    //metode

    public Employee(String firstName,String lastName,SalariedEmployee angajatSalariat,HourlyEmployee angajatOra,ComissionEmployee angajatComision){

        this.firstName = firstName;
        this.lastName = lastName;
        this.angajatSalariat = angajatSalariat;
        this.angajatOra = angajatOra;
        this.angajatComision = angajatComision;
    }
    public double getPaymentAmount(){
        return 0.0;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", angajatSalariat=" + angajatSalariat +
                ", angajatOra=" + angajatOra +
                ", angajatComision=" + angajatComision +
                '}';
    }
}
