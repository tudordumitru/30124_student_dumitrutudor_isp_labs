package isp.lab4.exercise4;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;
import isp.lab4.exercise3.Controler;

public class Exercise4 {
    public static void main(String[] args) {
        FireAlarm alarma = new FireAlarm(false);
        TemperatureSensor[] senzor = new TemperatureSensor[3];
        senzor[0] = new TemperatureSensor(41,"Bucatarie");
        senzor[1] = new TemperatureSensor(66,"Hol");
        senzor[2] = new TemperatureSensor(88,"Baie");

        Controler control = new Controler(senzor,alarma);
        control.controlStep();



    }
}
