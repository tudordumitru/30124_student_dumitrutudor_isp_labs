package isp.lab4.exercise3;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Controler {
    TemperatureSensor[] temperatureSensor= new TemperatureSensor[3];
    FireAlarm fireAlarm;

    //metode
    public void controlStep() {
        fireAlarm.setActive(false);
        for (int i = 0; i < 3; i++) {
            if (this.temperatureSensor[i].getValue() > 50)
                fireAlarm.setActive(true);


        }
        if(fireAlarm.isActive())
            System.out.println("Alarm started");
        else System.out.println("Alarm not started");
    }

    public Controler(TemperatureSensor[] t,FireAlarm f){
        temperatureSensor[0]=t[0];
        temperatureSensor[1]=t[1];
        temperatureSensor[2]=t[2];
        fireAlarm = f;
    }
    public static void main(String[] args) {
        FireAlarm alarma = new FireAlarm(false);
        TemperatureSensor[] senzor = new TemperatureSensor[3];
        senzor[0] = new TemperatureSensor(41,"Bucatarie");
        senzor[1] = new TemperatureSensor(66,"Hol");
        senzor[2] = new TemperatureSensor(88,"Baie");

        Controler control = new Controler(senzor,alarma);
        control.controlStep();



    }

    }

