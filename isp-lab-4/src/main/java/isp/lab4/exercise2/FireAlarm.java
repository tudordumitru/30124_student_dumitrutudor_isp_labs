package isp.lab4.exercise2;

import isp.lab4.exercise3.Controler;

public class FireAlarm  {
    private boolean active;

    //constructor
    public FireAlarm(boolean active){
        this.active=active;
    }

    //metode
    public boolean isActive(){
        return active;
    }
    public void setActive(boolean active){
        this.active=active;

    }

    @Override
    public String toString() {
        return "FireAlarm{" +
                "active=" + active +
                '}';
    }




}
