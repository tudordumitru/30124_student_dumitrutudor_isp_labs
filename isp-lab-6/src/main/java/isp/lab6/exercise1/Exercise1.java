package isp.lab6.exercise1;

import java.util.List;

public class Exercise1 {
    public static void main(String[] args) {
        System.out.println("Test implementation here.");
        SensorReadingList sensorReadingList = new SensorReadingList();

        SensorReading sensorReading1 = new SensorReading(2, "fasfa",Type.TEMPERATURE);
        sensorReadingList.addReading(sensorReading1);

        SensorReading sensorReading2 = new SensorReading(1, "fsafas",Type.TEMPERATURE);
        sensorReadingList.addReading(sensorReading2);

        SensorReading sensorReading3 = new SensorReading(3, "fsaggh",Type.TEMPERATURE);
        sensorReadingList.addReading(sensorReading3);

        SensorReading sensorReading4 = new SensorReading(0, "fasgb",Type.HUMIDITY);
        sensorReadingList.addReading(sensorReading4);

        System.out.println("All with type temperature: ");
        List<SensorReading> temperatures = sensorReadingList.getReadingsByType(Type.TEMPERATURE);
        for(SensorReading sensorReading : temperatures){
            System.out.println(sensorReading);
        }

        System.out.println("All with type temperature and location fsafas: ");
        List<SensorReading> temperatures2 = sensorReadingList.findAllByLocationAndType("fsafas", Type.TEMPERATURE);
        for(SensorReading sensorReading : temperatures2){
            System.out.println(sensorReading);
        }

        System.out.println("Sorted by location: ");
        sensorReadingList.listSortedByLocation();
        for(SensorReading sensorReading : sensorReadingList.getLista()){
            System.out.println(sensorReading);
        }

        System.out.println("Sorted by value: ");
        sensorReadingList.listSortedByValue();
        for(SensorReading sensorReading : sensorReadingList.getLista()){
            System.out.println(sensorReading);
        }
    }

}
