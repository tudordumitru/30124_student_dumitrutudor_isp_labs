package isp.lab6.exercise1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SensorReadingList implements IReadingRepository{
    private List<SensorReading> lista;

    public SensorReadingList() {
        lista = new ArrayList<SensorReading>();
    }

    public List<SensorReading> getLista() {
        return lista;
    }

    @Override
    public void addReading(SensorReading reading) {
        lista.add(reading);
    }

    @Override
    public double getAvarageValueByType(Type type, String location) {
        double sum = 0;
        double number = 0;
        for(SensorReading reading : lista)
        {
            if(reading.getType()==type&&reading.getLocation().equals(location)){
                sum += reading.getValue();
                number++;
            }
        }
        double average = sum / number;
        return average;
    }

    @Override
    public List<SensorReading> getReadingsByType(Type type) {
        List<SensorReading> readingsByType = new ArrayList<>();
        for(SensorReading reading : lista){
            if(reading.getType()==type)
                readingsByType.add(reading);
        }
        return readingsByType;
    }

    @Override
    public void listSortedByLocation() {
        lista.sort(new LocationComparator());
    }

    @Override
    public void listSortedByValue() {
        lista.sort(new ValueComparator());
    }

    @Override
    public List<SensorReading> findAllByLocationAndType(String location, Type type) {
        List<SensorReading> readingsByTypeAndLocation = new ArrayList<>();
        for(SensorReading reading : lista){
            if(reading.getType()==type&&reading.getLocation().equals(location))
                readingsByTypeAndLocation.add(reading);
        }
        return readingsByTypeAndLocation;
    }
}
