package isp.lab6.exercise3;


import java.util.ArrayList;

import java.util.List;

enum SensorType{
    TEMP, PRES;
}

class SensorReading implements Comparable<SensorReading>{

    double value;
    long dateAndTime;

    public SensorReading(double value, long dateAndTime) {
        this.value = value;
        this.dateAndTime = dateAndTime;
    }

    public double getValue() {
        return value;
    }

    public long getDateAndTime() {
        return dateAndTime;
    }

    @Override
    public int compareTo(SensorReading o) {
        return (int)(value - o.value);
    }
}
/////////////////////////////////////////
class Sensor{
    ArrayList<SensorReading> readings = new ArrayList<>();
    String id;
    SensorType type;

    public Sensor(String id, SensorType type) {
        this.id = id;
        this.type = type;
    }

    public boolean addSensorReading(SensorReading reading){
       if(!this.readings.contains((reading)))
           return false;
       readings.add(reading);
       return true;
    }

    List<SensorReading> getSensorReadingSortedByValue(){
        List<SensorReading> list = new ArrayList<>(this.readings);
        list.sort((o1, o2) -> (int) Math.signum(o1.getValue() - o2.getValue()));
        return list;
    }

}
/////////////////////////////////////////
class SensorCluster{
    ArrayList<Sensor> sensors = new ArrayList<>();

    public void addSensor(String id, SensorType type){
        sensors.add(new Sensor(id, type));
    }

    public boolean writeSensorReading(String id, double value, long dateTime){
        for(Sensor s: sensors){
            if(s.id.equals(id)){
                s.addSensorReading(new SensorReading(value,dateTime));
                return true;
            }
        }
        return false;
    }

    public Sensor getSensorById(String id){
        return sensors.stream().filter(s -> s.id==id).findFirst().get();
    }

}
/////////////////////////////////////////
public class Exercise3 {

    public static void main(String[] args) {
        SensorCluster c = new SensorCluster();
        c.addSensor("1",SensorType.TEMP);

        c.writeSensorReading("1",4,1);
        c.writeSensorReading("1",2,21);
        c.writeSensorReading("1",1,22);
        c.writeSensorReading("1",6,30);
        c.writeSensorReading("1",7,15);

        Sensor s1 = c.getSensorById("1");
        List<SensorReading> rez = s1.getSensorReadingSortedByValue();
        rez.stream().forEach(s -> System.out.println(s.value));




    }
}
