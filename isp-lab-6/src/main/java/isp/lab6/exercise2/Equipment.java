package isp.lab6.exercise2;


public class Equipment {

    private final String name;
    private final String serialNumber;
    private String owner;
    private boolean taken;


    public Equipment(String name, String serialNumber, String owner, boolean taken) {
        this.name = name;
        this.serialNumber = serialNumber;
        this.owner = owner;
        this.taken = taken;
    }

    public Equipment(String serialNumber) {

        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Equipment(String name, String serialNumber) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Equipment(String name, String serialNumber, String owner) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getName() {
        return name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getOwner() {
        return owner;
    }

    public boolean isTaken() {
        return taken;
    }

    /**
     * Provide the owner of the equipment
     * Equipment should be set as taken
     *
     * @param owner - owner name
     */
    public void provideEquipmentToUser(final String owner) {
        this.owner = owner;
        this.taken = true;
    }


    /**
     * Equipment is returned to the office
     * Equipment should not be set as taken
     * Remove the owner
     */
    public void returnEquipmentToOffice() {
        this.owner = null;
        this.taken = false;
    }
}
