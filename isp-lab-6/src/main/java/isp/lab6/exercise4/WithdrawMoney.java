package isp.lab6.exercise4;

public class WithdrawMoney extends Transaction{
    public double amount;

    public WithdrawMoney(Account account, double amount) {
        super(account);
        this.amount = amount;
    }

    @Override
    public String execute(){
        this.account.setBalance(this.account.getBalance() - this.amount);
        return "new balance : "+this.account.getBalance();
    }
}
