package isp.lab6.exercise4;

public class ChangePin extends Transaction{


    public String oldpin;
    public String newPin;

    public ChangePin(Account account, String oldpin, String newPin) {
        super(account);
        this.oldpin = oldpin;
        this.newPin = newPin;
    }
    
    @Override
    public String execute(){
        account.getCard().setPin(this.newPin);
        String s = "executed changing pin";
        return s;
    }
}
