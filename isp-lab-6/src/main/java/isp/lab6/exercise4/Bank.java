package isp.lab6.exercise4;

public class Bank {
    private int numberOfAccounts;
    private final Account[] accounts;

    public Bank() {
        this.numberOfAccounts = 0;
        this.accounts = new Account[11];
    }

    public void addAccount(Account account){
        this.accounts[numberOfAccounts] = new Account();
        this.accounts[numberOfAccounts] = account;
        this.numberOfAccounts++;
    }

    public String executeTransaction(Transaction t){
        return t.execute;
    }

    public Account getAccountByCardId(String cardId){
        for(int i = 0; i< accounts.length; i++){
            if(this.accounts[i].getCard().getCardId().equals(cardId))
                return this.accounts[i];
        }
        return null;
    }
}
