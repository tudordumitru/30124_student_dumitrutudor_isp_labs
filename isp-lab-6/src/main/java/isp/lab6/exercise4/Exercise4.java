package isp.lab6.exercise4;

public class Exercise4 {
    public static void main(String[] args) {
        Card c1 = new Card("1010101","2000");
        Card c2 = new Card("777","1111");
        Account a1 = new Account("Tudor",10000,c1);
        Account a2 = new Account("Iulia", 9999, c2);

        Bank bank = new Bank();
        bank.addAccount(a1);
        bank.addAccount(a2);

        ATM atm = new ATM(bank);
        atm.insertCard(c1, "2000");
        atm.insertCard(c2,"1111");

        atm.withdraw(696);
    }
}
