package isp.lab6.exercise4;

public class ATM {
    private Card card;
    private final Bank bank;

    public ATM(Bank bank){
        this.bank = bank;
    }

    public void changePin(String oldPin, String newPin){
        ChangePin pinChanger = new ChangePin(this.bank.getAccountByCardId(card.getCardId()),oldPin,newPin);
        pinChanger.execute();
    }

    public void withdraw(double amount){

        WithdrawMoney withdrawMoney = new WithdrawMoney(this.bank.getAccountByCardId(card.getCardId()),amount);
        withdrawMoney.execute();

    }

    public void insertCard(Card card, String pin){
        if(card!=null) {
            if (card.getPin().equals(pin)) {
                System.out.println("pin in correct");
                this.card = card;
            } else {
                System.out.println("pin not correct");
            }
        }
            else {
                System.out.println("card already inserted");
        }

        }



    public void checkMoney(){
        CheckMoney checkMoney = new CheckMoney(this.bank.getAccountByCardId(this.card.getCardId()));
        checkMoney.execute();
    }

    public void removeCard(){
        this.card = null;
    }
}
